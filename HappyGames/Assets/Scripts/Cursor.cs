﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;

namespace Assets.Scripts
{
    public class Cursor : MonoBehaviour
    {
        public Image Foreground;
        public RectTransform Body;
        public CanvasGroup Group;

        public Text XLabel;

        public Color OkColor;
        public Color WrongColor;

        public float SizeMultiplier = 1.3F;
        public float SizeTime = 0.3F;
        public AnimationCurve SizeCurve;

        public float AlphaTime = 0.2F;

        public float XStartTime = 0.4F;
        public float XFadeTime = 1.5F;
        public AnimationCurve XCurve;

        private Sequence _sequence;

        public void ShowOkAnimation()
        {
            Foreground.color = OkColor;

            _sequence = DOTween.Sequence();
            DefaultAnim( _sequence );
        }

        public void ShowWrongAnimation(Action<Cursor> onComplete)
        {
            Foreground.color = WrongColor;

            _sequence = DOTween.Sequence();
            DefaultAnim( _sequence );
    
            _sequence.InsertCallback( XStartTime, () => XLabel.gameObject.SetActive( true ) )
                .Insert( XStartTime, XLabel.DOFade( 0, XFadeTime ).SetEase( XCurve ) ).OnComplete( () =>
                {
                    XLabel.gameObject.SetActive( false );
                    onComplete.Invoke( this );
                } );
        }

        private void DefaultAnim( Sequence sequence)
        {
            var defaultSize = Body.localScale;

            Body.localScale = defaultSize * SizeMultiplier;
            Group.alpha = 0;

            sequence.Append( Body.DOScale( defaultSize, SizeTime ).SetEase( SizeCurve ) )
                .Insert( 0, Group.DOFade( 1, AlphaTime ) );
        }
    }
}