﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class GameEditor : MonoBehaviour
    {
        public Image Image1;
        public Image Image2;

        public LevelData CurrentLevel;

        public List<EditorArea> Areas;

        [System.Serializable] 
        public class EditorArea
        {
            public List<RectTransform> Transforms;
            public Color DebugColor;
        }

        void OnDrawGizmos()
        {
            if (Areas == null)
                return;
            foreach (var area in Areas)
            {
                if (area.Transforms.Count < 2)
                    continue;
           
                Gizmos.color = area.DebugColor;

                for (int i = 0; i < area.Transforms.Count; i++)
                {
                    var next = i + 1 >= area.Transforms.Count ? 0 : i + 1;
                    Gizmos.DrawLine( area.Transforms[i].position, area.Transforms[next].position);
                }
            }
        }

        [ContextMenu("Save")]
        public void Save()
        {
            CurrentLevel.Areas = new Area[Areas.Count];
            for (int i = 0; i < Areas.Count; i++)
            {
                CurrentLevel.Areas[i] =
                    new Area() {Points = Areas[i].Transforms.Select( x => x.anchoredPosition ).ToArray() };
            }
            EditorUtility.SetDirty( CurrentLevel );
            AssetDatabase.SaveAssets();
        }

        [ContextMenu( "Load" )] 
        public void Load()
        {
            foreach (var editorArea in Areas)
            {
                DestroyImmediate( editorArea.Transforms[0].parent.gameObject );
            }

            Areas.Clear();
            var levelAreas = CurrentLevel.Areas;
            var i = 0;
            foreach (var area in levelAreas)
            {
                var editorArea = new EditorArea();
                editorArea.DebugColor = new Color32( (byte)Random.Range( 0, 255 ), (byte)Random.Range( 0, 255 ),
                    (byte)Random.Range( 0, 255 ), 255 );
                editorArea.Transforms = new List<RectTransform>();
                var parentGo = new GameObject(i.ToString(), typeof(RectTransform));
                i++;
                var parent = parentGo.GetComponent<RectTransform>();
                parent.SetParent( Image1.rectTransform, true );
                parent.anchorMin = Vector2.zero;
                parent.anchorMax = Vector2.one;
                parent.anchoredPosition = Vector2.zero;
                parent.sizeDelta = Vector2.zero;
                parent.localScale = Vector3.one;

                foreach (var point in area.Points)
                {
                    var go = new GameObject("Point", typeof(RectTransform));
                    var tr = go.GetComponent<RectTransform>();
                    tr.SetParent( parent, true );
                    tr.anchoredPosition = point;
                    editorArea.Transforms.Add( tr );
                }

                Areas.Add( editorArea );
            }

            Image1.sprite = CurrentLevel.Image1;
            Image2.sprite = CurrentLevel.Image2;
        }
    }
}
