﻿using UnityEngine;

namespace Assets.Scripts
{
    [CreateAssetMenu(menuName = "Create Level Data")]
    public class LevelData : ScriptableObject
    {
        public Sprite Image1;
        public Sprite Image2;

        public Area[] Areas;
    }
}
