﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pooling;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Game : MonoBehaviour
    {
        private const string OK_LABEL_FORMAT = "Отличий найдено: <color=green>{0}</color>";
        private const string WRONG_LABEL_FORMAT = "Ошибок совершено: <color=red>{0}</color>";

        public Image Image1;
        public Image Image2;

        public Text OkLabel;
        public Text WrongLabel;

        private LevelData _current;

        public LevelData[] _levels;

        private List<Area> _notFoundAreas;

        public Cursor CursorPrefab;
        public RectTransform CursorParent;

        private ObjectPool<Cursor> _cursorPool;

        private int _wrongCounter = 0;
        

        void Start()
        {
            SetLevel( _levels[0] );
        }

        public void SetLevel( LevelData level )
        {
            if(_cursorPool != null)
                _cursorPool.PushAll();

            _wrongCounter = 0;

            Image1.sprite = level.Image1;
            Image2.sprite = level.Image2;

            _current = level;
            _notFoundAreas = new List<Area>(_current.Areas);
        }

        void Update()
        {
            if (_current == null)
                return;

            if (Input.GetMouseButtonDown( 0 ))
            {
                var images = new[] {Image1, Image2};

                Image successImage = null;
                Image castedImage = null;
                foreach (var image in images)
                {
                    if( !RectTransformUtility.RectangleContainsScreenPoint( image.rectTransform, Input.mousePosition, Camera.main) )
                        continue;

                    castedImage = image;

                    if (CastToImage( image, out var area ))
                    {
                        _notFoundAreas.Remove( area );
                        successImage = image;
                        break;
                    }
                }

                if (successImage)
                {
                    ShowCursor( Input.mousePosition, true, successImage );
                }
                else if(castedImage != null)
                {
                    ShowCursor( Input.mousePosition, false, castedImage );
                    _wrongCounter++;
                }
            }

            OkLabel.text = string.Format( OK_LABEL_FORMAT, _current.Areas.Length - _notFoundAreas.Count );
            WrongLabel.text = string.Format( WRONG_LABEL_FORMAT, _wrongCounter );

            if (_notFoundAreas.Count == 0)
            {
                StartCoroutine( DoNextLevel(_current) );
                _current = null;
            }
           
        }

        IEnumerator DoNextLevel(LevelData prev)
        {
            yield return new WaitForSeconds( 1.5F );
            SetNextLevel(prev);
        }

        public void SetNextLevel(LevelData prev)
        {
            var levelIdx = System.Array.IndexOf( _levels, prev );
            levelIdx++;
            if (levelIdx >= _levels.Length)
            {
                Application.Quit();
            }
            else
            {
                SetLevel( _levels[levelIdx] );
            }
        }

        public void ShowCursor(Vector2 position, bool isCorrect, Image sender)
        {
            Vector2 localPoint;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle( sender.rectTransform, Input.mousePosition,
                Camera.main, out localPoint ))
            {
                if (isCorrect)
                {
                    SpawnCursor( localPoint, true, Image1.rectTransform );
                    SpawnCursor( localPoint, true, Image2.rectTransform );
                }
                else
                {
                    SpawnCursor( localPoint, false, sender.rectTransform );
                }
            }
        }

        private void SpawnCursor( Vector2 position, bool isCorrect, RectTransform parent )
        {
            if(_cursorPool == null)
                _cursorPool = new ObjectPool<Cursor>( CursorPrefab, 0, CursorParent );

            var cursor = _cursorPool.Pop();
            cursor.transform.SetParent( parent, true );
            cursor.Body.anchoredPosition = position;
            if(isCorrect)
                cursor.ShowOkAnimation();
            else
                cursor.ShowWrongAnimation((c) => _cursorPool.Push( c ));
        }

        bool CastToImage( Image image, out Area castedArea )
        {
            castedArea = null;
            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle( image.rectTransform, Input.mousePosition,
                Camera.main, out var localPoint ))
                return false;

            var imageScale = image.rectTransform.sizeDelta.y / image.sprite.rect.height;

            foreach (var area in _notFoundAreas)
            {
                var contains = Poly.ContainsPoint( area.Points.Select( x => x * imageScale ).ToArray(), localPoint ); //да, я знаю, что прогонять linq в апдейте, да еще и в циклах тяжелая операция, но ведь и задача оптимизации не стояла)
                if (contains)
                {
                    castedArea = area;
                    return true;
                }  
            }

            return false;
        }
    }
}
