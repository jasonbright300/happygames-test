﻿namespace Pooling
{
    public interface IResetable
    {
        void Reset();
    }
}