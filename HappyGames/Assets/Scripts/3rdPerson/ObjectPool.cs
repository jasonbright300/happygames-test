﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pooling
{
    /// <summary>
    /// Универсальный пулл объектов.
    /// 
    /// Может быть использован вне менеджера пуллов, но требует его существования в сцене,
    /// поскольку сам не является MonoBehaviour и соответственно не может инстансить объекты.
    /// 
    /// Author: Timur Vorobyov aka Jason Bright
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ObjectPool<T> where T : Component
    {
        #region Public Properties
        /// <summary>
        /// Префаб пулла
        /// </summary>
        public T Prefab { get; private set; }

        public Transform Parent { get; private set; }

        /// <summary>
        /// ОБЩИЙ текущий размер пулла.
        /// Внимание: изменение размера в меньшую сторону приведет к уничтожению объектов пулла, но
        /// вначале будут удалены неиспользуемые в данный момент объекты.
        /// </summary>
        public int Capacity
        {
            get { return _pulledObjects.Count; }
            set
            {
                var val = Mathf.Clamp(value, 0, int.MaxValue);

                while (Capacity < val)
                {
                    CreateObject();
                }

                while (Capacity > val)
                {
                    if (_inactiveObjects.Count > 0)
                        RemoveObject(_inactiveObjects.Dequeue());
                    else
                        RemoveObject(_pulledObjects[0]);
                }
            }
        }

        /// <summary>
        /// Количество неиспользуемых объектов в данный момент
        /// </summary>
        public int InactiveCount { get { return _inactiveObjects.Count; } }
        #endregion

        private List<T> _pulledObjects = new List<T>();
        private Queue<T> _inactiveObjects = new Queue<T>();

        #region Public Methods
        public ObjectPool(T prefab, int capacity = 0, Transform parent = null)
        {
            Parent = parent;
            Prefab = prefab;
            Capacity = capacity;
        }

        /// <summary>
        /// Добавить объект в пулл.
        /// Объект будет автоматически выключен.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="waitAnimation"></param>
        public void Push(T obj, bool waitAnimation = false)
        {
            if (obj == null)
                return;

            //if (waitAnimation)
            //{
            //    var animateable = obj.GetComponent<IAnimateable>();
            //    if (animateable != null)
            //    {
            //        animateable.OnPushed();
            //        GameRoot.Instance.StartCoroutine( DoWaitObjectAnimation( obj, animateable ) ); //[JB] это залупень, что корутина запускается на геймруте. Надо сделать отдельный объект для корутин.
            //        return;
            //    }
            //}

            if (!_pulledObjects.Contains(obj))
                _pulledObjects.Add(obj);

            _inactiveObjects.Enqueue(obj);
            obj.transform.SetParent( Parent, false );
            obj.gameObject.SetActive( false );

            var reset = obj.GetComponent<IResetable>();
            if (reset != null)
            {
                reset.Reset();
            }
        }

        //IEnumerator DoWaitObjectAnimation( T obj, IAnimateable animation )
        //{
        //    while (animation.IsPlaying)
        //        yield return null;

        //    Push( obj, false );
        //}

        /// <summary>
        /// Добавить массив объектов в пулл.
        /// Объект будет автоматически выключен.
        /// </summary>
        /// <param name="objects"></param>
        public void Push( T[] objects )
        {
            foreach (var obj in objects)
            {
                Push( obj );
            }
        }
        /// <summary>
        /// Взять объект из пулла.
        /// Объект будет автоматически включен.
        /// </summary>
        /// <returns></returns>
        public T Pop(bool autoEnable = true)
        {
            if (_inactiveObjects.Count == 0)
            {
                Capacity++;
            }

            var obj = _inactiveObjects.Dequeue();
            //теоретически в пулле могут быть уничтоженные объекты, поэтому надо найти существующий объект
            if (obj == null)
                obj = Pop(); //да, рекурсия. Максимальная глубина: количество неактивных объектов в пуле.

            if(autoEnable)
                obj.gameObject.SetActive( true );
            return obj;
        }

        /// <summary>
        /// Удалить все объекты из пулла
        /// </summary>
        public void Clear()
        {
            Capacity = 0;
        }

        /// <summary>
        /// Возвращает все активные объекты в пулл
        /// </summary>
        public void PushAll()
        {
            var active = _pulledObjects.Except( _inactiveObjects );
            foreach (var activeObj in active)
            {
                Push( activeObj );
            }
        }
        #endregion

        public IEnumerator DoCreateObject(int capacity)
        {
            for (int i = 0; i < capacity; i++)
            {
                CreateObject();
                yield return null;
            }         
        }

        private T CreateObject()
        {
            var obj = GameObject.Instantiate( Prefab ) as T; // ObjectPoolManager.Instance.InstantiateObject(Prefab) as T;
            Push(obj);
            return obj;
        }

        private void RemoveObject(T obj)
        {
            if (obj == null)
                return;

            _pulledObjects.Remove(obj);
            GameObject.Destroy(obj.gameObject);
            //ObjectPoolManager.Instance.DestroyGameObject();
        }
    }
}
